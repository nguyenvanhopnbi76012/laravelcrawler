<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Goutte;
use App\Models\Post;


class scrapeDanTri extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'scrape:dantri';

    protected $cat = [
        // 'su-kien.htm',
        // 'the-gioi.htm',
        'the-thao.htm',
        'lao-dong-viec-lam.htm'
    ];



    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        foreach ($this->cat as $cat) {
            $crawler = Goutte::request('GET', 'https://dantri.com.vn/' . $cat);
            $linkPost = $crawler->filter('.news-item__title>a')->each(function ($node) {
                return $node->attr('href');
            });

            foreach ($linkPost as $link) {
                // print(env('DAN_TRI') . $link . "\n");
                // scrapePost(env("DAN_TRI") . $link);
                $l =env('DAN_TRI') . $link;
                self::scrapePost($l);
            }
        }



    }

    public static function scrapePost($url){
        $crawler = Goutte::request('GET', $url);
        $title = $crawler->filter('.dt-news__title')->each(function ($node) {
            return $node->text();
        });
        $slug = '';

        if(isset($title[0])){
            $title = $title[0];
            $slug = str_slug($title);
        }



        $des = $crawler->filter('.dt-news__sapo h2')->each(function ($node) {
            return $node->text();
        });

        if(isset($des[0]))
            $des = $des[0];

        $content = $crawler->filter('.dt-news__content')->each(function ($node) {
            return $node->text();
        });

        if(isset($content[0]))
            $content = $content[0];



        $thumbnail = $crawler->filter('.dt-news__content img')->each(function ($node) {
            return $node->attr('src');
        });

        if(isset($thumbnail[0]))
            $thumbnail = $thumbnail[0];


        $data = [
            'title' => $title,
            'slug' => $slug,
            'thumbnail' => $thumbnail,
            'description' => $des,
            'content' => $content
        ];

        Post::create($data);

        print('Lay du lieu thanh cong' . "\n");
    }
}
